## Game 2048
2048 is a popular puzzle game in which the main aim is to join the tiles with same numbers until we reach 2048.

## Description
This project deals with a simple puzzle game 2048 which is built by using basic javascript, HTML and CSS.
- Link for the game to start playing: https://we-game-2048.netlify.app/ <br />

This is a single player game where one can play as many times as he/she can. It's main aim is to join the same numbers until we get 2048 in any one of the tile.

Here are some of the javascript Methods used in this project:
- querySelector()
- getElementById()
- createElement()
- appendChild()
- push()
- Math.floor()
- Math.random()
- length
- innerHTML
- parseInt()
- filter()
- Array()
- fill()
- concat()
- keyCode

## Visuals
![screenshot](image.png)

## Installation
### Clone the repository:
- Clone or download the repository 'game 2048' and open 'index.html' and start playing.

or

- visit the link: https://we-game-2048.netlify.app/

## How to play
1. Move the tiles with same numbers up, down, left and right by using arrow keys in such a way until we reach 2048.
2. Tiles with different numbers on it cannot be added 
3. Make sure that the whole grid is not filled with the numbers others than '0' else the game will be over.

## References
JavaScript Documentation: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide


